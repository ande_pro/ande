package com.ande;

public class Record {
    private String title;
    private String register;
    private String price;
    private String name;
    private String tender;
    private String exchange;
    private String balance;

    @Override
    public String toString() {
        return "Record{" +
                "title='" + title + '\'' +
                ", register='" + register + '\'' +
                ", price='" + price + '\'' +
                ", name='" + name + '\'' +
                ", tender='" + tender + '\'' +
                ", exchange='" + exchange + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTender() {
        return tender;
    }

    public void setTender(String tender) {
        this.tender = tender;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
