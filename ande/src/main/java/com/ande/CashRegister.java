package com.ande;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class CashRegister {

    private static String welCome = "WelCome";

    private static String register = "Please enter cash register's float:";
    private static String name = "Please enter the item's name:";

    private static String price = "Please enter the item's cost:";

    private static String tender = "Please enter the cash amount tendered:";


    private static String exchange = "Amount of change required = $";
    private static String balanceText = "Balance of the Cash Register:$";

    private static String write = "Do you want to write file,Please input yes or no?";

    private static String isContinue = "Do you continue exchange,Please input yes or no?";

    private static String finish = "Exchange finished!";

    private static String error = "Number Input Error!";


    public static void main(String[] args) throws Exception{

        initLocalEnvironment();

        String s, c, isOk,isWrite;
        double balance;

        Scanner in = new Scanner(System.in);

        System.out.println(welCome);

        try {
            while (true) {
                Record record = new Record();
                record.setTitle(welCome);
                System.out.print(register);

                s = in.nextLine();
                balance = Double.parseDouble(s);
                record.setRegister(register +s);

                System.out.print(name);
                s = in.nextLine();
                record.setName(name+s);
                System.out.print(price);
                c = in.nextLine();
                record.setPrice(price+s);

                Transaction trans = new Transaction(s, Double.parseDouble(c));

                System.out.print(tender);
                s = in.nextLine();
                record.setTender(tender+s);
                c = Double.toString(Double.parseDouble(s) - trans.getCost());

                System.out.println(exchange + c);
                record.setExchange(exchange+c);
                c = Double.toString(balance + trans.getCost());
                System.out.println(balanceText + c);
                record.setBalance(balanceText+c);
                System.out.println(write);
                isWrite = in.nextLine();
                if (isWrite.equals("yes")) {
                    writeFile(record.toString());
                }

                System.out.println(isContinue);
                isOk = in.nextLine();
                if (isOk.equals("no")) {
                    System.out.println(finish);
                    break;
                }
            }
        } catch (NumberFormatException e) {
            System.out.println(error);
        }

    }

    /**
     * 写文件
     * @param text
     * @throws Exception
     */
    private static void  writeFile(String text) throws Exception{
        File file = new File(new File("").getAbsolutePath()+File.separator+"file");
        if (!file.exists()) {
            file.mkdir();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String dateStr = simpleDateFormat.format(date);
        String name = file.getAbsolutePath()+File.separator +dateStr+".txt";
        File file1 = new File(name);
        FileWriter fw = new FileWriter(file1);
        fw.write(text);//将字符串写入到指定的路径下的文件中
        fw.close();
    }

    /**
     * 初始化英文环境
     */
    private static void initLocalEnvironment() {

        //取得系统 指定的 en 环境
//        Locale locale = Locale.getDefault();
        Locale locale = new Locale("en", "US");
        //根据指定国家/语言环境加载资源文件
        ResourceBundle bundle = ResourceBundle.getBundle("message/message", locale);
        //从资源文件中取得的消息
        welCome = bundle.getString("welCome");
        register = bundle.getString("register");
        name = bundle.getString("name");
        price = bundle.getString("price");
        tender = bundle.getString("tender");
        exchange = bundle.getString("exchange");
        balanceText = bundle.getString("balanceText");

        isContinue = bundle.getString("isContinue");
        finish = bundle.getString("finish");
        error = bundle.getString("error");
    }
}
